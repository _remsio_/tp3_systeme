#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from threading import Thread
from threading import Semaphore
from random import *
import time

"""
@note: Module contenant la classe Lecteur lisant un message publicitaire

@authors: Rémi MATASSE : gr TD1
"""

class Lecteur(Thread) :
    """
        Classe représentant un Lecteur quelconque
    """

    def __init__(self, messagePublicitaire, nomduLecteur):
        Thread.__init__(self)
        self.messagePublicitaire = messagePublicitaire
        self.nomduLecteur = nomduLecteur

    def run(self):
        """Code à exécuter pendant l'exécution du thread."""

        iteration = 1
        while(iteration < 6):
            # On s'assure qu'un message existe
            if(self.messagePublicitaire.tableau[iteration % self.messagePublicitaire.taille] != ""):

                self.messagePublicitaire.lecture(iteration % self.messagePublicitaire.taille, self.nomduLecteur)
                iteration +=1

            time.sleep(2)
