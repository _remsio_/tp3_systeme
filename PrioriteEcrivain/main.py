from MessagePublicitaire import MessagePublicitaire
from Lecteur import Lecteur
from Ecrivain import Ecrivain
from threading import Semaphore
from termcolor import colored

semaphoreProtectionCompteur = Semaphore()
semaphorePossedeInfo = Semaphore()

messagePublicitaire = MessagePublicitaire(3, semaphoreProtectionCompteur, semaphorePossedeInfo)

print(colored("\n------------------------------------------", "cyan"))
print(colored("Version laissant la priorite aux ecrivains", "cyan"))
print(colored("------------------------------------------\n", "cyan"))

lecteur1 = Lecteur(messagePublicitaire, "lecteur1")
lecteur1.start()
lecteur2 = Lecteur(messagePublicitaire, "lecteur2")
lecteur2.start()
lecteur3 = Lecteur(messagePublicitaire, "lecteur3")
lecteur3.start()

ecrivain1 = Ecrivain(messagePublicitaire, "ecrivain1")
ecrivain1.start()
ecrivain2 = Ecrivain(messagePublicitaire, "ecrivain2")
ecrivain2.start()
ecrivain3 = Ecrivain(messagePublicitaire, "ecrivain3")
ecrivain3.start()
