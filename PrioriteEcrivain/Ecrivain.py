#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from threading import Thread
from threading import Semaphore
from random import *
import time

"""
@note: Module contenant la classe Ecrivain écrivant ou
        mettant à jour un message publicitaire

@authors: Rémi MATASSE : gr TD1
"""

class Ecrivain(Thread) :
    """
        Classe représentant un Ecrivain quelconque
    """

    def __init__(self, messagePublicitaire, nomDeLecrivain):
        Thread.__init__(self)
        self.messagePublicitaire = messagePublicitaire
        self.nomDeLecrivain = nomDeLecrivain

    def run(self):
        """Code à exécuter pendant l'exécution du thread."""

        numberMessage = 1
        while(numberMessage < 6):
            message = self.messagePublicitaire.tableau[numberMessage % self.messagePublicitaire.taille] + str(numberMessage)
            self.messagePublicitaire.ecriture(message, numberMessage % self.messagePublicitaire.taille, self.nomDeLecrivain)
            numberMessage += 1
            time.sleep(1)
