#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@note: Module contenant la classe MessagePublicitaire étant un message partagé
        entre un lecteur et un rédacteur.

@authors: Rémi MATASSE : gr TD1
"""

import time
from termcolor import colored

class MessagePublicitaire :
    """
        Classe représentant un tableau de message publicitaire
    """

    def __init__(self, taille, semaphoreProtectionCompteur, semaphorePossedeInfo):
        self.tableau = [""] * taille
        self.taille = taille
        self.semaphoreProtectionCompteur = semaphoreProtectionCompteur
        self.semaphorePossedeInfo = semaphorePossedeInfo

        self.nombreEcrivains = 0

    def lecture(self, position, nomduLecteur):
        """
            Méthode de lecture du MessagePublicitaire
        """
        self.semaphorePossedeInfo.acquire()
        print(colored("[+] Debut lecture {} à la position [{}]".format(nomduLecteur, position), "green"))
        print(colored("[*] {} lit {}".format(nomduLecteur, self.tableau[position]), "green"))
        print(colored("[-] Fin lecture {}".format(nomduLecteur), "green"))
        self.semaphorePossedeInfo.release()

    def ecriture(self, message, position, nomDeLecrivain):
        """
            Méthode d'écriture du MessagePublicitaire
        """
        self.semaphoreProtectionCompteur.acquire()
        self.nombreEcrivains += 1
        print(colored("++ Nombre ecrivains voulant acceder a la ressource {} ++ ".format(self.nombreEcrivains), "blue"))
        if self.nombreEcrivains == 1 :
            self.semaphorePossedeInfo.acquire()
        print(colored("[+] Debut ecriture {}".format(nomDeLecrivain), "red"))
        print(colored("[*] {} écris: {} à la position [{}]".format(nomDeLecrivain, message, position), "red"))
        self.tableau[position] = message
        self.semaphoreProtectionCompteur.release()

        self.semaphoreProtectionCompteur.acquire()
        self.nombreEcrivains -= 1
        print(colored("-- Nombre ecrivains voulant acceder a la ressource  {} --".format(self.nombreEcrivains), "blue"))
        if self.nombreEcrivains == 0 :
            self.semaphorePossedeInfo.release()
        print(colored("[-] Fin ecriture {}".format(nomDeLecrivain), "red"))
        self.semaphoreProtectionCompteur.release()
