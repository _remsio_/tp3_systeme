# Le modèle lecteur(s)-rédacteur
Les lecteurs peuvent consulter l'information qu’un rédacteur
construit.
Les contraintes sont les suivantes :
 - IL n'y a pas de limite sur le nombre de lecteurs en parallèle
 - Les lectures s'effectuent  en exclusion mutuelle avec le rédacteur.